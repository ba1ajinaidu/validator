import sys

def validate_filter_schema(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        is_valid_schema = False

        for line in lines:
            stripped_line = line.strip()

            # Ignore comments and empty lines
            if stripped_line.startswith("#") or stripped_line == "":
                continue

            if stripped_line.startswith("filter {"):
                is_valid_schema = True
            elif not is_valid_schema:
                return False
            elif is_valid_schema and stripped_line == "}":
                return True
            
        return False

def main():
    for file_path in sys.argv[1:]:
        if not validate_filter_schema(file_path):
            print(f"Validation failed for file: {file_path}")
            sys.exit(1)
        print(f"Validaton passed for: {file_path}")

    print("Validation passed for all filter files.")

if __name__ == "__main__":
    main()
